package runnables;

import communication.PortCommunication;
import server.receiveComm.advanced.AServerSkeleton;

public class startServerAdvanced {
    public static void main(String[] args) {
        var serverSkeleton = new AServerSkeleton();

        var portComm = new PortCommunication();
        portComm.createPort(serverSkeleton.getPort());

        serverSkeleton.run();
    }
}
