package runnables;

import communication.PortCommunication;
import server.receiveComm.ServerSkeleton;

public class startServer {
    public static void main(String[] args) {
        var serverSkeleton = new ServerSkeleton();

        var portComm = new PortCommunication();
        portComm.createPort(serverSkeleton.getPort());

        serverSkeleton.run();
    }
}
