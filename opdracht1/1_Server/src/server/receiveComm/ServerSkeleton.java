package server.receiveComm;

import client.documents.DocumentImpl;
import client.documents.sendComm.DocumentStub;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;
import server.Server;
import server.ServerImpl;

public class ServerSkeleton {
    private MessageManager messageManager;
    private Server server;

    public ServerSkeleton() {
        this.messageManager = new MessageManager();
        System.out.println("my address = " + this.messageManager.getMyAddress());
        this.server = new ServerImpl();
    }

    public int getPort() {
        return this.messageManager.getMyAddress().getPortNumber();
    }

    private void sendEmptyReply(MethodCallMessage request) {
        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
        response.setParameter("emptyResponse", "ServerSkeleton-Ok");
        this.messageManager.send(response, request.getOriginator());
    }

    private void handleRequest(MethodCallMessage request) {
        switch (request.getMethodName()) {
            case "log":
                this.handleLog(request);
                break;
            case "create":
                this.handleCreate(request);
                break;
            case "toUpper":
                this.handleToUpper(request);
                break;
            case "toLower":
                this.handleToLower(request);
                break;
            case "type":
                this.handleType(request);
                break;
            default:
                this.sendEmptyReply(request);
        }

    }

    private void handleLog(MethodCallMessage request) {
        //* Receive Message
        var text = request.getParameter("text");
        var document = new DocumentImpl(text);

        //* Call Method on real obj
        this.server.log(document);

        //* Sync
        this.sendEmptyReply(request);
    }

    private void handleCreate(MethodCallMessage request) {
        //* Receive Message
        var document = request.getParameter("document");

        //* Call Method on real obj
        var responseDoc = this.server.create(document);

        //* Sync
        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
        response.setParameter("response", responseDoc.getText());
        this.messageManager.send(response, request.getOriginator());
    }
    //? ----------------- Without Connection -----------------

    //    private void handleToUpper(MethodCallMessage request) {
//        //* Receive Message
//        var document = new DocumentImpl(request.getParameter("document"));

//        //* Call Method on real obj
//        this.server.toUpper(document);
//
//        //* Sync
//        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
//        response.setParameter("response", document.getText());
//        this.messageManager.send(response, request.getOriginator());
//    }
//
//    private void handleToLower(MethodCallMessage request) {
//        //* Receive Message
//        var document = new DocumentImpl(request.getParameter("document"));
//
//        //* Call Method on real obj
//        this.server.toLower(document);
//
//        //* Sync
//        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
//        response.setParameter("response", document.getText());
//        this.messageManager.send(response, request.getOriginator());
//    }
    //? ---------------------------------------------------

    //* With connection works as well but:
    //* pro: Works the same as before but now instantly changes the file that is on the client.
    //* con: Should take more time, since for each change you send requests to Client, While before you sent only one request to commit changes.
    //* This action of sending requests can be very taxing for connection if there were to be more files and more interchanges.

    //? ----------------- With Connection -----------------
    private void handleToUpper(MethodCallMessage request) {
        //* Receive Message
        var documentAddress = this.getAddress(request);

        //* Call Method on real obj
        var document = new DocumentStub(documentAddress);
        this.server.toUpper(document);

        //* Sync
        this.sendEmptyReply(request);
    }

    private void handleToLower(MethodCallMessage request) {
        //* Receive Message
        var documentAddress = this.getAddress(request);

        //* Call Method on real obj
        var document = new DocumentStub(documentAddress);
        this.server.toLower(document);

        //* Sync
        this.sendEmptyReply(request);
    }

    //? ---------------------------------------------------
    private void handleType(MethodCallMessage request) {
        //* Receive Message
        var documentAddress = this.getAddress(request);
        var document = new DocumentStub(documentAddress);
        var text = request.getParameter("text");


        //* Call Method on real obj
        this.server.type(document, text);

        //* Sync
        this.sendEmptyReply(request);
    }

    private NetworkAddress getAddress(MethodCallMessage request) {
        var ip = request.getParameter("IP");
        var port = request.getParameter("Port");
        return new NetworkAddress(ip, Integer.parseInt(port));
    }

    public void run() {
        while (true) {
            var request = messageManager.wReceive();
            this.handleRequest(request);
        }
    }
}
