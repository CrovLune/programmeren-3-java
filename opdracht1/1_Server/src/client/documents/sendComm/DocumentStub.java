package client.documents.sendComm;

import client.documents.Document;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public class DocumentStub implements Document {
    private final NetworkAddress documentAddress;
    private final MessageManager messageManager;

    public DocumentStub(NetworkAddress documentAddress) {
        this.documentAddress = documentAddress;
        this.messageManager = new MessageManager();
    }

    private synchronized void checkEmptyReply() {
        String value = "";
        while (!value.equals("DocumentSkeleton - Ok")) {
            MethodCallMessage response = this.messageManager.wReceive();
            if (!response.getMethodName().equals("response")) {
                continue;
            }
            value = response.getParameter("emptyResponse");
        }
    }


    @Override
    public synchronized String getText() {
            //* Create message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "getText");

            //* Send message to real Client
            this.messageManager.send(request, this.documentAddress);

            //* Make this sync
            var response = messageManager.wReceive().getParameter("document");
            return response;
    }

    @Override
    public synchronized void setText(String text) {
            //* Create message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "setText");
            request.setParameter("text", text);

            //* Send message to real Client
            this.messageManager.send(request, this.documentAddress);

            //* Make this sync
            this.checkEmptyReply();
    }

    @Override
    public synchronized void append(char c) {
            //* Create message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "append");
            request.setParameter("char", (String.valueOf(c)));

            //* Send message to real Client
            this.messageManager.send(request, this.documentAddress);

            //* Make this sync
            this.checkEmptyReply();
    }

    @Override
    public synchronized void setChar(int position, char c) {
            //* Create message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "setChar");
            request.setParameter("position", String.valueOf(position));
            request.setParameter("char", String.valueOf(c));

            //* Send message to real Client
            this.messageManager.send(request, this.documentAddress);

            //* Make this sync
            this.checkEmptyReply();
    }
}
