package Runnables;

import client.Client;
import client.documents.DocumentImpl;
import client.documents.receiveComm.DocumentSkeleton;
import communication.NetworkAddress;
import communication.PortCommunication;
import server.Server;
import server.sendComm.ServerStub;

public class StartDistributed {
    //* Read Port from File created by Server
    private final static PortCommunication portCommunication = new PortCommunication();
    private final static int port = portCommunication.readPort();
    private final static NetworkAddress serverAdress = new NetworkAddress("192.168.0.101", port);

    public static void main(String[] args) {
        DocumentImpl document = new DocumentImpl();

        DocumentSkeleton documentSkeleton = new DocumentSkeleton(document);
        new Thread(documentSkeleton).start();

        Server server = new ServerStub(serverAdress, documentSkeleton.getAddress());

        Client client = new Client(server, document);
        client.run();
    }
}
