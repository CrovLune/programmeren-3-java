package Runnables;

import client.advanced.AClient;
import client.documents.DocumentImpl;
import client.documents.receiveComm.DocumentSkeleton;
import communication.NetworkAddress;
import communication.PortCommunication;
import server.Server;
import server.sendComm.advanced.AServerStub;

public class StartAdvanced {
    private final static PortCommunication portCommunication = new PortCommunication();
    private final static int port = portCommunication.readPort();
    private final static NetworkAddress serverAdress = new NetworkAddress("192.168.0.101", port);

    public static void main(String[] args) {
        DocumentImpl document = new DocumentImpl();

        DocumentSkeleton documentSkeleton = new DocumentSkeleton(document);
        new Thread(documentSkeleton).start();

        Server server = new AServerStub(serverAdress, documentSkeleton.getAddress());

        AClient client = new AClient(server, document);
        client.run();
    }
}
