package client.documents.receiveComm;

import client.documents.Document;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public class DocumentSkeleton implements Runnable {
    private MessageManager messageManager;
    private Document document;

    public DocumentSkeleton(Document document) {
        this.messageManager = new MessageManager();
        System.out.println("my address = " + this.messageManager.getMyAddress());
        this.document = document;
    }

    public NetworkAddress getAddress() {
        return this.messageManager.getMyAddress();
    }

    private void sendEmptyReply(MethodCallMessage request) {
        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
        response.setParameter("emptyResponse", "DocumentSkeleton - Ok");
        this.messageManager.send(response, request.getOriginator());
    }

    private void handleRequest(MethodCallMessage request) {
        switch (request.getMethodName()) {
            case "getText":
                this.handleGetText(request);
                break;
            case "setText":
                this.handleSetText(request);
                break;
            case "append":
                this.handleAppend(request);
                break;
            case "setChar":
                this.handleSetChar(request);
                break;
            default:
                this.sendEmptyReply(request);
        }

    }

    private void handleSetChar(MethodCallMessage request) {
        //* Receive Message
        var c = request.getParameter("char").charAt(0);
        var pos = Integer.parseInt(request.getParameter("position"));

        //* Call method on real obj
        this.document.setChar(pos, c);

        //* Sync
        this.sendEmptyReply(request);
    }

    private void handleAppend(MethodCallMessage request) {
        //* Receive Message
        var string = request.getParameter("char");
        var c = string.charAt(0);

        //* Call method on real obj
        this.document.append(c);

        //* Sync
        this.sendEmptyReply(request);
    }

    private void handleSetText(MethodCallMessage request) {
        //* Receive Message
        var text = request.getParameter("text");

        //* Call method on real obj
        this.document.setText(text);

        //* Sync
        this.sendEmptyReply(request);
    }

    private void handleGetText(MethodCallMessage request) {
        //* Receive Message
        // request parameters are empty

        //* Call method on real obj
        var text = this.document.getText();

        //* Sync
        var response = new MethodCallMessage(this.messageManager.getMyAddress(), "response");
        response.setParameter("document", text);

        this.messageManager.send(response, request.getOriginator());
        this.sendEmptyReply(request);
    }

    public void run() {
        while (true) {
            var request = messageManager.wReceive();
            this.handleRequest(request);
        }
    }
}
