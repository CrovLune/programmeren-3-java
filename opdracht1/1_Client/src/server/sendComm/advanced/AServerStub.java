package server.sendComm.advanced;

import client.documents.Document;
import client.documents.DocumentImpl;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;
import server.Server;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AServerStub implements Server {
    private NetworkAddress serverAddress;
    private MessageManager messageManager;
    private NetworkAddress documentAddress;

    public AServerStub(NetworkAddress serverAddress, NetworkAddress documentAddress) {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
        this.documentAddress = documentAddress;
    }

    private void checkEmptyReply() {
        String value = "";
        while (!value.equals("Ok")) {
            MethodCallMessage response = this.messageManager.wReceive();
            if (!response.getMethodName().equals("response")) {
                continue;
            }
            value = response.getParameter("response");
        }
    }

    @Override
    public void log(Document document) {
        CompletableFuture.runAsync(() -> {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                    //* Create message
                    var request = new MethodCallMessage(this.messageManager.getMyAddress(), "log");
                    request.setParameter("text", document.getText());

                    //* Send message to real Server
                    this.messageManager.send(request, this.serverAddress);

                    //* Make this async
                    this.checkEmptyReply();
                }
        );
    }

    @Override
    public Document create(String document) {
        var doc =
                CompletableFuture.supplyAsync(() -> {
                    //* Create message
                    var request = new MethodCallMessage(this.messageManager.getMyAddress(), "create");
                    request.setParameter("document", document);

                    //* Send message to real Server
                    this.messageManager.send(request, this.serverAddress);

                    //* Make this async
                    var response = messageManager.wReceive().getParameter("response");
                    return new DocumentImpl(response);
                });

        try {
            System.out.println("DOC.GET: " + doc.get().getText());
            return doc.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        notify();
        return new DocumentImpl("NOT OK");
    }

    @Override
    public void toUpper(Document document) {
        CompletableFuture.runAsync(() -> {
            //* Create Message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toUpper");
            request.setParameter("IP", this.documentAddress.getIpAddress());
            request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));

            //* Send message to Real Server
            this.messageManager.send(request, this.serverAddress);

            //* Make this sync
            this.checkEmptyReply();
        });
        notify();
    }

    @Override
    public void toLower(Document document) {
        CompletableFuture.runAsync(() -> {
            //* Create Message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toLower");
            request.setParameter("IP", this.documentAddress.getIpAddress());
            request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));

            //* Send message to Real Server
            this.messageManager.send(request, this.serverAddress);

            //* Make this sync
            this.checkEmptyReply();
        });
        notify();
    }


    @Override
    public void type(Document document, String text) {
        CompletableFuture.runAsync(() -> {
            //* Create Message
            var request = new MethodCallMessage(this.messageManager.getMyAddress(), "type");
            request.setParameter("IP", this.documentAddress.getIpAddress());
            request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));
            request.setParameter("text", text);

            //* Send message to Real Server
            this.messageManager.send(request, this.serverAddress);

            //* Make this sync
            this.checkEmptyReply();
//            var response = this.messageManager.wReceive();
//            document.setText(response.getParameter("response"));
        });
        notify();
    }
}
