package server.sendComm;

import client.documents.Document;
import client.documents.DocumentImpl;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;
import server.Server;

public class ServerStub implements Server {
    private NetworkAddress serverAddress;
    private MessageManager messageManager;
    private NetworkAddress documentAddress;

    public ServerStub(NetworkAddress serverAddress, NetworkAddress documentAddress) {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
        this.documentAddress = documentAddress;
    }

    private void checkEmptyReply() {
        String value = "";
        while (!value.equals("ServerSkeleton-Ok")) {
            MethodCallMessage response = this.messageManager.wReceive();
            if (!response.getMethodName().equals("response")) {
                continue;
            }
            value = response.getParameter("emptyResponse");
        }
    }

    @Override
    public void log(Document document) {
        //* Create message
        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "log");
        request.setParameter("text", document.getText());

        //* Send message to real Server
        this.messageManager.send(request, this.serverAddress);

        //* Make this sync
        this.checkEmptyReply();
    }

    @Override
    public Document create(String document) {
        //* Create message
        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "create");
        request.setParameter("document", document);

        //* Send message to real Server
        this.messageManager.send(request, this.serverAddress);

        //* Make this sync
        var response = messageManager.wReceive().getParameter("response");
        return new DocumentImpl(response);
    }

    //* Old
//    @Override
//    public void toUpper(Document document) {
//        //* Create Message
//        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toUpper");
//        request.setParameter("document", document.getText());
//
//        //* Send message to Real Server
//        this.messageManager.send(request, this.serverAddress);
//
//        //* Make this sync
//        var response = this.messageManager.wReceive();
//        document.setText(response.getParameter("response"));
//    }
//
//    @Override
//    public void toLower(Document document) {
//        //* Create Message
//        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toLower");
//        request.setParameter("document", document.getText());
//
//        //* Send message to Real Server
//        this.messageManager.send(request, this.serverAddress);
//
//        //* Make this sync
//        var response = this.messageManager.wReceive();
//        document.setText(response.getParameter("response"));
//    }

    //* new
    @Override
    public void toUpper(Document document) {
        //* Create Message
        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toUpper");
        request.setParameter("IP", this.documentAddress.getIpAddress());
        request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));

        //* Send message to Real Server
        this.messageManager.send(request, this.serverAddress);

        //* Make this sync
        this.checkEmptyReply();
    }

    @Override
    public void toLower(Document document) {
        //* Create Message
        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "toLower");
        request.setParameter("IP", this.documentAddress.getIpAddress());
        request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));

        //* Send message to Real Server
        this.messageManager.send(request, this.serverAddress);

        //* Make this sync
        this.checkEmptyReply();
    }


    @Override
    public void type(Document document, String text) {
        //* Create Message
        var request = new MethodCallMessage(this.messageManager.getMyAddress(), "type");
        request.setParameter("IP", this.documentAddress.getIpAddress());
        request.setParameter("Port", String.valueOf(this.documentAddress.getPortNumber()));
        request.setParameter("text", text);

        //* Send message to Real Server
        this.messageManager.send(request, this.serverAddress);

        //* Make this sync
        this.checkEmptyReply();
    }
}
