package be.kdg.distrib.reflection;

import be.kdg.distrib.communication.MethodCallMessage;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class Decoder {
    public static Object primeToObject(Class<?> returnType, String text) {
        switch (returnType.getTypeName()) {
            case "boolean":
                return Boolean.parseBoolean(text);
            case "char":
                return text.charAt(0);
            case "byte":
                return Byte.parseByte(text);
            case "short":
                return Short.parseShort(text);
            case "int":
                return Integer.parseInt(text);
            case "long":
                return Long.parseLong(text);
            case "float":
                return Float.parseFloat(text);
            case "double":
                return Double.parseDouble(text);
            default:
                return text;
        }
    }

    public static Object resolveReturnMessage(MethodCallMessage response, Method method) {
        Class<?> returnType = method.getReturnType();

        if (Encoder.isPrime(returnType)) {
            return primeToObject(returnType, response.getParameter("result"));
        } else {
            return resolveObject(returnType, response);
        }
    }

    public static Object resolveObject(Class<?> returnType, MethodCallMessage response) {
        Object objectImpl = null;
        Map<String, String> parameters = response.getParameters();

        try {
            objectImpl = returnType.getDeclaredConstructor().newInstance();

            for (Map.Entry<String, String> param : parameters.entrySet()) {
                String key = param.getKey();
                String value = param.getValue();

                int levels = key.split("\\.").length;

                if (levels == 1) {
                    continue;
                }
                Field field = objectImpl.getClass().getDeclaredField(key.split("\\.")[1]);// result.name -> name
                field.setAccessible(true);
                field.set(objectImpl, primeToObject(field.getType(), value));

            }
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return objectImpl;
    }
}
