package be.kdg.distrib.reflection;

import be.kdg.distrib.communication.MethodCallMessage;
import java.lang.reflect.Method;
import java.util.*;

public class Checker {

    public static Method doesMethodExistInRealObject(MethodCallMessage message, Object object) {
        for (Method method : object.getClass().getMethods()) {
            if (method.getName().equals(message.getMethodName())) {
                return method;
            }
        }
        return null;
    }

    public static void checkArgumentsCount(MethodCallMessage message, Method method) {
        int argumentsInOriginal = method.getParameterCount();

        int argumentsSupplied;
        Set<Map.Entry<String, String>> methodParameters = message.getParameters().entrySet();

        TreeMap<String, String> resultMap = new TreeMap<>();

        for (Map.Entry<String, String> param : methodParameters) {
            if (!param.getKey().startsWith("arg")) throw new RuntimeException();

            String[] keys = param.getKey().split("\\.");
            if (keys.length > 1) {
                resultMap.put(keys[0], param.getValue());
            } else {
                resultMap.put(param.getKey(), param.getValue());
            }
        }
        argumentsSupplied = resultMap.keySet().size();

        if (argumentsSupplied != argumentsInOriginal) throw new RuntimeException();
    }

    public static List<Object> getMessageArguments(MethodCallMessage message, Method method) {
        int amountOfParametersInMethod = method.getParameterCount();
        Class<?>[] parameterTypes = method.getParameterTypes();

        List<Object> objects = new ArrayList<>();

        for (int i = 0; i < amountOfParametersInMethod; i++) {
            Map<String, String> methodArgs = message.getParametersStartingWith(String.format("arg%d", i));

            if (methodArgs.size() < 2) {
                objects.add(Decoder.primeToObject(parameterTypes[i], methodArgs.get(String.format("arg%d", i))));
            } else {
                objects.add(Decoder.resolveObject(parameterTypes[i], message));
            }
        }
        return objects;
    }
}
