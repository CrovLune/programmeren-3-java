package be.kdg.distrib.reflection;

import be.kdg.distrib.communication.MethodCallMessage;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Encoder {
    public static void resolveArguments(Object[] args, MethodCallMessage message) {
        TreeMap<String, String> tree = new TreeMap<>();

        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                String index = String.format("arg%d", i);
                tree.putAll(resolveArgument(index, args[i], tree));
            }
        }
        tree.forEach(message::setParameter);
    }

    public static TreeMap<String, String> resolveArgument(String index, Object obj, TreeMap<String, String> tree) {
        if (isPrime(obj)) {
            tree.put(index, String.valueOf(obj));
        } else {
            tree.putAll(checkObject(index, obj, tree));
        }
        return tree;
    }

    public static TreeMap<String, String> checkObject(String index, Object obj, TreeMap<String, String> tree) {
        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field field : fields) {
            String fieldName = field.getName();
            String newIndex = String.format("%s.%s", index, fieldName);

            field.setAccessible(true);

            Object fieldValue = null;
            try {
                fieldValue = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if (fieldValue != null && isPrime(field.getType())) {
                tree.put(newIndex, fieldValue.toString());
            } else {
                tree.putAll(resolveArgument(newIndex, fieldValue, tree));
            }
        }

        return tree;
    }

    public static Boolean isPrime(Object object) {
        List<Class<?>> primitives = new ArrayList<>();

        primitives.add(Boolean.class);
        primitives.add(Character.class);
        primitives.add(Byte.class);
        primitives.add(Short.class);
        primitives.add(Integer.class);
        primitives.add(Long.class);
        primitives.add(Float.class);
        primitives.add(Double.class);
        primitives.add(Void.class);
        primitives.add(String.class);

        if (object instanceof Class) {
            return ((Class<?>) object).isPrimitive() || primitives.contains(object);
        } else {
            return object.getClass().isPrimitive() || primitives.contains(object.getClass());
        }
    }
}
