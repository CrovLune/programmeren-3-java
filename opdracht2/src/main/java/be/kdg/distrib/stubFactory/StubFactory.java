package be.kdg.distrib.stubFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class<?> interfaceClass, String ipAddress, int portNumber) {
        InvocationHandler handler = new StubInvocationHandler(ipAddress, portNumber);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{interfaceClass}, handler);
    }
}
