package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.reflection.Decoder;
import be.kdg.distrib.reflection.Encoder;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class StubInvocationHandler implements InvocationHandler {
    private final NetworkAddress networkAddress;
    private final MessageManager messageManager;
    private final NetworkAddress myAddress;

    public StubInvocationHandler(String ipAddress, int portNumber) {
        this.messageManager = new MessageManager();
        this.networkAddress = new NetworkAddress(ipAddress, portNumber);
        this.myAddress = messageManager.getMyAddress();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args){
        String methodName = method.getName();
        MethodCallMessage message = new MethodCallMessage(myAddress, methodName);

        Encoder.resolveArguments(args, message);
        messageManager.send(message, networkAddress);

        MethodCallMessage response = messageManager.wReceive();
        Object returnMessage = Decoder.resolveReturnMessage(response, method);

        if (method.getReturnType() == Void.class) {
            return null;
        } else {
            return returnMessage;
        }
    }
}
