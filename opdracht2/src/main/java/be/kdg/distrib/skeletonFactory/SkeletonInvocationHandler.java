package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.reflection.Checker;
import be.kdg.distrib.reflection.Encoder;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.TreeMap;

public class SkeletonInvocationHandler implements InvocationHandler {
    private final MessageManager messageManager;
    private final Object object;

    public SkeletonInvocationHandler(Object object) {
        this.messageManager = new MessageManager();
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        switch (method.getName()) {
            case "run":
                run();
                break;
            case "getAddress":
                return messageManager.getMyAddress();
            case "handleRequest":
                handleRequest((MethodCallMessage) args[0]);
                break;
        }
        return null;
    }

    private void handleRequest(MethodCallMessage message) {
        List<Object> arguments;
        try {
            Method method = Checker.doesMethodExistInRealObject(message, this.object);
            if (method == null) throw new RuntimeException();

            arguments = resolveArguments(message, method);
            Object obj = method.invoke(object, arguments.toArray());
            MethodCallMessage result = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
            TreeMap<String, String> resultParameters = new TreeMap<>();
            if (obj == null) {
                resultParameters.put("result", "Ok");
            } else {
                Encoder.resolveArgument("result", obj, resultParameters);
            }

            resultParameters.forEach(result::setParameter);
            messageManager.send(result, message.getOriginator());
        } catch (IllegalAccessException | InvocationTargetException | NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void run() {
        Runnable runnable = () -> {
            while (true) {
                MethodCallMessage message = messageManager.wReceive();
                if (message != null) {
                    handleRequest(message);
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private List<Object> resolveArguments(MethodCallMessage message, Method method) {
        Checker.checkArgumentsCount(message, method);
        return Checker.getMessageArguments(message, method);
    }
}
