package be.kdg.distrib;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.junit.runners.Suite.*;

@RunWith(Suite.class)
@SuiteClasses({
        TestStubFactory.class,
        TestSkeletonFactory.class
})
public class TestSuite {
}
